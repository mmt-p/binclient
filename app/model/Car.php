<?php
// namespace app\model;

class Car
{
    protected static $data_file;
    protected $inventory = [];
    
    public function __construct(){
        self::$data_file = DATA . 'car.txt';
    }
    
    private function load(){
        if (file_exists(self::$data_file)) {
            $this->inventory = file(self::$data_file);
        }
    }
    
    public function getCars(){
        $this->load();
        return $this->inventory;
    }
}

