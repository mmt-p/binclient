<?php
// namespace app\model;

class User
{   
    public function table(){
        return TABLE_PREFIX . 'user';
    }

    public function getUser($email){
        $res = Db::start()->select()
                        ->from('user')
                        ->where(
                            [
                                ['email', '"' . $email . '"']
                            ])
                        ->run();
        return $res ? (object)$res[0] : null;
    }
    
    public function getUsers(){
        return $db = Db::start()->select()
                        ->from('user')
                        ->where(
                            [
                                ['id', '>', '2'], 
                                ['last_name','"Manam"']
                            ])
                        ->run();
    }

    public function checkPass($hash, $pass, $salt){
        //Hash pass and compare
        return ($hash === $pass ? true : false);
    }

    public function isAdmin(){
        return $_SESSION['identity']->user_type == 20 ? true : false;
    }
}

