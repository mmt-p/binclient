<?php
//May be modified
define('BASE_DIR', '/binclient');
define('TABLE_PREFIX', '');


//Not to be modified
define('ROOT', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);
define('VIEW', APP . 'view' . DIRECTORY_SEPARATOR);
define('MODEL', APP. 'model' . DIRECTORY_SEPARATOR);
define('DATA', APP . 'data' . DIRECTORY_SEPARATOR);
define('CORE', APP . 'core' . DIRECTORY_SEPARATOR);
define('CONTROLLER', APP . 'controller'. DIRECTORY_SEPARATOR);

//paths
define('WEB', ROOT . 'public' . DIRECTORY_SEPARATOR);
define('CSS', WEB . 'css' . DIRECTORY_SEPARATOR);
define('JS', WEB . 'js' . DIRECTORY_SEPARATOR);
define('IMG', WEB . 'img' . DIRECTORY_SEPARATOR);
define('ASSESSMENT_IMAGES', IMG . 'assessment_images' . DIRECTORY_SEPARATOR);

//URLs
define('BASE_URL', BASE_DIR);
define('CSS_URL', '');


//Load running configs
define('RUN', require 'running.php');