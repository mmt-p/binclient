<?php
return [
	'db' => [
        'host' => 'mysql:host=localhost',
        'name' => 'binclient',
        'user' => 'manamRoot',
        'password' => 'root2018',
	],
	//Set free-access allowed controller actions
	'allow' => [
		//'Controller' => ['actions']
		'User' => [
			'login','passReset',
		],
		'Home' => [
			'index'
		],
		'Images' => [
			'available','get_image'
		],
	],

	'clients_mapping' => [
		//'servename' => ['configs']
		'localhost' => [
			'images'	=> 	'sanlam',
			'key'	=> 	'localhost',
			'passcode'	=> 	'localXYZ',
		],
		'sanlam.innovexsolutions.co.ke' => [
			'images','sanlam',
			'key','sanlam',
			'passcode','sanlam1234',
		],
	],
];