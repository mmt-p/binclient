<?php
require 'consts.php';

$modules = [ROOT, APP, DATA, CORE, CONTROLLER];

set_include_path(get_include_path() . PATH_SEPARATOR . implode(PATH_SEPARATOR, $modules));

spl_autoload_register(
	function ($class_name) {
	    include $class_name . '.php';
	}, true);