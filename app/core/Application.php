<?php 

class Application
{
	protected $controller;
	protected $action;
	protected $name;
	protected $params = [];

	public function __construct()
	{
		// echo Controller::getUser();die();
		session_start();

		$this->prepareURL();

		if(file_exists(CONTROLLER . $this->controller . '.php')){

		    $this->controller = new $this->controller;

		    if (method_exists($this->controller, $this->action)){
		    	if($this->assertAccess()){//confirm access
		    		call_user_func_array([$this->controller, $this->action], $this->params);
		    		exit();
		    	} 

		    	//Redirect to login
	    		ob_start();
			    header('Location: ' . Utils::myURL('user.login'));
			    ob_end_flush();
			    exit();

		    } else {
		    	echo 'Action ' . $this->action . ' not found';
		    }
		} else {
			echo 'Controller ' . $this->controller . ' not found';
		}
	}
	
	protected function prepareURL(){
	    $request = trim($_SERVER['REQUEST_URI'], '/');
	    
        $url = explode('/', $request);

        //Trim the base directory path
        $base_url = trim(BASE_URL, '/');

        $base_path = explode('/', $base_url);

        for ($i = 0; $i < count($base_path); $i ++) {
        	if ($base_path[$i]) {
        		unset($url[$i]);
        	}
        }

        $url = array_values($url); //Reset index to 0

        $this->name = (isset($url[0]) && $url[0] ) ? ucfirst($url[0]) : 'Home';
        $this->controller = $this->name . 'Controller';
        $this->action = isset($url[1]) ? $url[1] : 'index';
        unset($url[0], $url[1]);
        $this->params = !empty($url) ? array_values($url) : [];	    
	}

	protected function assertAccess(){
		return ($this->controller->isLogged() || $this->isAllowed()) ? true: false ;
	}

	protected function isAllowed(){
		//Check if controller action free access allowed
		return (!in_array($this->name, RUN['allow']) && in_array($this->action, RUN['allow'][$this->name], 1)) ? true : false;
	}
}