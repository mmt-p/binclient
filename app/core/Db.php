<?php 

class Db
{
	protected $pdo;
	protected $sql;
	protected $config;
	protected $add_type;
	protected const ADD_TYPES = array('INSERTING' => 1, 'UPDATING' => 2);

	public function __construct()
	{
		$config = (object)RUN['db'];
		try{
		    $this->pdo = new PDO($config->host . ';dbname=' . $config->name, $config->user, $config->password);
		    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $this->sql = '';
		    $this->add_type = NULL;
		    $this->config = $config;
		} catch (PDOException $pe) {
		    echo "DATABASE ERROR: connection fail\n" . $pe; 
		    // error_log("\n\nDATABASE ERROR: connection fail\n\n" . $pe , 3, DB_ERR);
		}
	}

	public static function start() {
		return new Db();
	}

	protected function end() {
		$this->pdo = null;
		$this->sql = null;
		$this->config = null;
	}

	public function getSQL() {
		return $this->sql;
	}

	public function from($table) {
		$this->sql .= ' FROM ' . $table;
		return $this;
	}

	public function select($fields = '') {
		$this->sql .= 'SELECT ' . ($fields ? $fields : '*') . ' ';
		return $this;
	}

	public function into($table) {
		$this->sql .= ' INTO ' . $table;
		return $this;
	}

	public function insert($table) {
		$this->add_type = Db::ADD_TYPES['INSERTING'];
		$this->sql .= 'INSERT INTO '  . $table. '(';
		return $this;
	}

	public function update($table) {
		$this->add_type = Db::ADD_TYPES['UPDATING'];
		$this->sql .= 'UPDATE '  . $table. ' SET ';
		return $this;
	}

	public function string($fieldsString, $valuesString) {
		$this->sql .= $fieldsString . ') VALUES (' . $valuesString . ')' ;
		return $this;
	}

	public function multiArrays($fieldsArray, $valuesArray) {
		$fieldsString = '';
		$valuesString = '';
		$updateString = ' ';
		for ($i=0; $i < count($fieldsArray) ; $i++) {
			if ($this->add_type === Db::ADD_TYPES['INSERTING']) {
				$fieldsString .= $fieldsArray[$i] . ',';
				$valuesString .= $valuesArray[$i] . ',';
			} else if($this->add_type === Db::ADD_TYPES['UPDATING']) {
				$updateString .= $fieldsArray[$i] . '=' . $valuesArray[$i] . ',';
			}
		}
		$this->sql .= ( ($this->add_type === Db::ADD_TYPES['INSERTING']) ? trim($fieldsString, ',') . ') VALUES (' . trim($valuesString, ',') . ')' : trim($updateString,','));
		return $this;
	}

	public function singleArray($keyValueArray) {
		$fieldsString = '';
		$valuesString = '';
		$updateString = ' ';
		foreach ($keyValueArray as $field => $value) {
			if ($this->add_type === Db::ADD_TYPES['INSERTING']) {
				$fieldsString .= $field . ',';
				$valuesString .= $value . ',';
			} else if($this->add_type === Db::ADD_TYPES['UPDATING']) {
				$updateString .= $field . '=' . $value . ',';
			}
		}
		$this->sql .= ( ($this->add_type === Db::ADD_TYPES['INSERTING']) ? trim($fieldsString, ',') . ') VALUES (' . trim($valuesString, ',') . ')' : trim($updateString,','));
		return $this;
	}

	public function where($conditions = []) {
		$this->sql .= ' WHERE 1 = 1 ';
		foreach ($conditions as $condition) {			
			$count = count($condition);
			if ($count) {
				if ($count == 2) {
					$this->sql .= ' AND ' . $condition[0] . ' = ' . $condition[1];
				} else if ($count == 3) {
					$this->sql .= ' AND ' . $condition[0] . ' ' . $condition[1] . ' ' . $condition[2];
				} else {
					echo 'Wrong where clause format';
				}
			}
		}
		return $this;
	}

	public function run($array = true) {
	    $res = array(); 
	    try {
	        $stmt = $this->pdo->prepare($this->getSQL());
	        $stmt->execute();
	        if (!$this->add_type) {
	        	if ($array) {
		            $stmt->setFetchMode(PDO::FETCH_ASSOC);
		        }
		        $res = $stmt->fetchAll();
	        } else {
	        	if ($this->add_type === Db::ADD_TYPES['INSERTING']) {
	        		$res = $this->pdo->lastInsertId();
	        	} else {
	        		$res = $stmt->rowCount();
	        	}
	        }
	    } catch (PDOException $pe) {
	        $stmt = null;
	        echo "\n\nDATABASE ERROR:\n" . $pe . "\n\n";
	    }
	    //Close connection
	    $this->end();

	    return $res;
	}
}