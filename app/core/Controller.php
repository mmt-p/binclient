<?php
// namespace app\core;

class Controller
{
    protected $view; 
    protected $model;
    protected static $user;

    public function view($viewName, $data=[], $title = ''){
          $this->view = new View($viewName, $data, $title);
          return $this->view;
    }
    
    public function model($modelName, $data=[]){
        if (file_exists(MODEL . $modelName . '.php')){
            require MODEL . $modelName . '.php';
            $this->model = new $modelName;
        } else {
            echo 'Model ' . MODEL . $modelName . '.php not found';
        }
    }

    public function isLogged(){
        return isset($_SESSION["is_logged"]) ? $_SESSION["is_logged"] : false;
    }

    public function setIdentity($user){
        
        Controller::$user = $user;

        session_start();
        $_SESSION["is_logged"] = true;
        $_SESSION["identity"] = $user;
    }

    public function getIdentity(){
        return Controller::$user;
    }    
}

