<?php 

class Utils
{
	public function myURL($path='', $params=[]){
		if (!$path) { //Defaults to home page
			$path = 'home.index';
		}

      	$parts = explode('.', $path);

      	//If action missing, go to index
      	$url = '/' . $parts[0] . '/' . (isset($parts[1]) ? $parts[1] : 'index');

      	foreach ($params as $param) {
      		$url .= '/' . $param;
      	}

      	return BASE_URL === '/' ? $url : BASE_URL . $url;
	}

      public function reRoute($url){
            //Redirect to the given url
            ob_start();
            header('Location: ' . $url);
            ob_end_flush();
            exit();
      }
}