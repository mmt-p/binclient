<?php
// namespace app\view;

class View
{
    protected $view_file;
    protected $view_data;
    protected $page_title;
    protected $final_view;

    public function __construct($view_file, $view_data, $title = ''){
        $this->view_file = $view_file;
        $this->view_data = $view_data;
        $this->final_view = 'final';

        ($title ? $this->setTitle($title) : $this->defaultTitle());
    }
    
    public function render(){
        if (file_exists(VIEW . $this->final_view . '.phtml')){            
            include VIEW . $this->final_view . '.phtml';
        } else {
            echo 'Sorry, final view ' . $this->final_view . ' not found';
        }
    }
    
    public function getAction(){
        return (explode('/', $this->view_file))[1];
    }

    public function getController(){
        return (explode('/', $this->view_file))[0];
    }
    
    public function setTitle($title = ''){
        $this->page_title =  $title;
    }
    
    public function getTitle(){
        return $this->page_title;
    }

    public function defaultTitle(){
        $title = explode('/', $this->view_file);

        $this->page_title = ucfirst($title[0]) . '-' . ucfirst($title[1]);
    }
}

