<?php
// namespace app\controller;

// use Controller;

class CarController extends Controller
{ 
    public function index(){
        $this->model('Car'); 
        $this->view('car' . DIRECTORY_SEPARATOR . 'index',['cars' => $this->model->getCars()]);
        $this->view->render();
    }
}

