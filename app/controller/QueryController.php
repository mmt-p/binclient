<?php
// namespace app\controller;

class QueryController extends Controller
{
    public function index(){
        $this->view('query/index', [], 'New Queries');
        $this->view->render();
    }

    public function add(){
        $this->view('query/add', [], 'Add Query');
        $this->view->render();
    }

    public function history(){
        $this->view('query/history', [], 'Query History');
        $this->view->render();
    }
}

