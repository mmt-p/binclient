<?php
// namespace app\controller;

// use Controller;

class UserController extends Controller
{ 
    public function __construct()
    {
        $this->model('User');        
    }

    public function login(){

        if ($this->isLogged()) {
            $this->model->isAdmin() ? Utils::reRoute(Utils::myURL('query.history')) : Utils::reRoute(Utils::myURL('query'));
        }

        if ($_POST) {
            
            $input = (object)$_POST;

            $user = $this->model->getUser($input->uname);

            if ($user) { //User exist

                if ($this->model->checkPass($user->hash, $input->psw, $user->salt)) {
                    $this->setIdentity($user);//Pass actual user data
                    Utils::reRoute(Utils::myURL('user.login'));
                } else { //Wrong password error

                }
                
            } else { //No such account error

            }
        }

        Utils::reRoute(Utils::myURL('home.index',['show_login'=>true]));
    }

    public function logout(){
        session_destroy();
        Utils::reRoute(Utils::myURL());
    }

    public function passReset(){
    	echo 'Reseting passcode';
    }

    public function profile(){
    	$this->view('user/profile', [], 'User Profile');
        $this->view->render();
    }

    public function index(){
        $this->view('user/index', ['users' => $this->model->getUsers()], 'All Users');
        $this->view->render();
    }
}

