<?php

class ImagesController extends Controller
{
    public function __construct()
    {
        $this->model('User');        
    }

    public function available(){

        if (!$_POST) {
            echo 'Required parameters missing';
            exit();
        }
        
        $system = isset(RUN['clients_mapping'][$_POST['system']]['images']) ? RUN['clients_mapping'][$_POST['system']]['images'] : false;

        if ($system) {
            $salvage = strtoupper(str_replace(' ','_', $_POST['salvage']));
            $dir = ASSESSMENT_IMAGES . $system . '/' . $salvage . '/';

            if (is_dir($dir)) {
                if ($dh = opendir($dir)) {
                    $files = array();

                    while (($file = readdir($dh)) !== false) { 
                        if (filetype($dir . $file) == 'file') {
                            array_push($files, $file);
                        }
                    }

                    closedir($dh);

                    if ($files) {
                        echo json_encode($files);
                    } else {
                        echo 'No images found';
                    }
                } else {
                    echo 'Failed to open remote directory';
                }
            } else {
                echo "Specified salvage images are missing";
            } 
        } else {
            echo 'Sorry, unknown system ' . strtoupper($_POST['system']) . ' specified';
        }
    }

    public function get_image(){

        if (!$_POST) {
            echo 'Required parameters missing';
            exit();
        }

        $system = isset(RUN['clients_mapping'][$_POST['system']]['images']) ? RUN['clients_mapping'][$_POST['system']]['images'] : false;

        if ($system) {
            $salvage = strtoupper(str_replace(' ','_', $_POST['salvage']));
            $file = ASSESSMENT_IMAGES . $system . '/' . $salvage . '/' . $_POST['image'];
            if (is_file($file)) {
                if ($file_data = file_get_contents($file)) {
                    echo $file_data;
                } else {
                    echo json_encode(array('Failed to open remote file'));
                }
            } else {
                echo json_encode(array("File not specified"));
            }            
        } else {
            echo json_encode(array('Sorry, unknown system ' . strtoupper($_POST['system']) . ' specified'));
        }
    }
}

