<?php
// namespace app\controller;

class HomeController extends Controller
{
    public function index($show_login = false){
        $this->view('home/index',
            ['show_login'=>$show_login], 'Home');
        $this->view->render();
    }
    
    public function aboutUs(){
        $this->view('home/aboutUs');
        $this->view->setTitle('This is about us page');
        $this->view->render();
    }
}

